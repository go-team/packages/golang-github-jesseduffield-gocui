Source: golang-github-jesseduffield-gocui
Section: golang
Priority: optional
Maintainer: Debian Go Packaging Team <team+pkg-go@tracker.debian.org>
Uploaders: Jongmin Kim <jmkim@debian.org>
Rules-Requires-Root: no
Build-Depends: debhelper-compat (= 13),
               dh-sequence-golang,
               golang-any,
               golang-github-gdamore-tcell.v2-dev,
               golang-github-go-errors-errors-dev,
               golang-github-mattn-go-runewidth-dev,
               golang-github-rivo-uniseg-dev,
               golang-github-stretchr-testify-dev <!nocheck>
Standards-Version: 4.7.2
Homepage: https://github.com/jesseduffield/gocui
Vcs-Browser: https://salsa.debian.org/go-team/packages/golang-github-jesseduffield-gocui
Vcs-Git: https://salsa.debian.org/go-team/packages/golang-github-jesseduffield-gocui.git
XS-Go-Import-Path: github.com/jesseduffield/gocui
Testsuite: autopkgtest-pkg-go

Package: golang-github-jesseduffield-gocui-dev
Architecture: all
Depends: ${misc:Depends},
         golang-github-gdamore-tcell.v2-dev,
         golang-github-go-errors-errors-dev,
         golang-github-mattn-go-runewidth-dev,
         golang-github-rivo-uniseg-dev
Description: minimalist console user interfaces Go library
 This package provides the minimalist Go package aimed at creating the
 Console User Interfaces.
 .
 Following features are available:
   * Minimalist API
   * Views (the "windows" in the GUI) implement the interface io.ReadWriter
   * Support for overlapping views
   * The GUI can be modified at runtime (concurrent-safe)
   * Global and view-level keybindings.
   * Mouse support
   * Colored text
   * Customizable edition mode
   * Easy to build reusable widgets, complex layouts
