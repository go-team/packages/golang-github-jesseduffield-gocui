Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: gocui
Upstream-Contact: Jesse Duffield <jessedduffield@gmail.com>
Source: https://github.com/jesseduffield/gocui
Files-Excluded: _examples/Mark.Twain-Tom.Sawyer.txt
Comment:
 File is licensed under the Project Gutenberg license, and it's been concluded
 that it is not compliant with the DFSG. Refer to the following for more info:
 https://lists.debian.org/debian-legal/2025/03/msg00006.html
 https://lists.debian.org/debian-legal/2017/08/msg00001.html

Files: *
Copyright:
 2014-2020 The gocui Authors
 2018-2024 Jesse Duffield <jessedduffield@gmail.com>
License: BSD-3-clause

Files: debian/*
Copyright: 2019-2025 Jongmin Kim <jmkim@debian.org>
License: BSD-3-clause
Comment: Debian packaging is licensed under the same terms as upstream

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
     * Redistributions of source code must retain the above copyright
       notice, this list of conditions and the following disclaimer.
     * Redistributions in binary form must reproduce the above copyright
       notice, this list of conditions and the following disclaimer in the
       documentation and/or other materials provided with the distribution.
     * Neither the name of the gocui Authors nor the names of its contributors
       may be used to endorse or promote products derived from this software
       without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
